﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

namespace library.Models
{
    public class BookModels
    {
         // ID книги
        public int Id { get; set; }
        // название книги
        public string Name { get; set; }
        // автор книги
        public string Author { get; set; }
        // издательство
        public string Publisher { get; set; }
        // серия
        public int Series { get; set; }
        // год издания
        public int Year { get; set; }
        // ISBN ¯\_(ツ)_/¯ 
        public int ISBN { get; set; }
        // Переводчик
        public string Translator { get; set; }
        // количество страниц
        public int NumOfPages { get; set; }
        // Формат
        public string Format { get; set; }
        // тип обложки
        public string CoverType { get; set; }
        // тираж
        public int Circulation { get; set; }
        // вес, г
        public int Weight { get; set; }
        // возрастные ограничения
        public int AgeRestrictions { get; set; }
    }
}