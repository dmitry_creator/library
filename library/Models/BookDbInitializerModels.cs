﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace library.Models
{
    public class BookDbInitializer : DropCreateDatabaseAlways<BookContextModels>
    {
        protected override void Seed(BookContextModels db)
        {
            db.Books.Add(new BookModels { Name = "Война и мир", Author = "Л. Толстой", Publisher = "АСТ" });
            db.Books.Add(new BookModels { Name = "Отцы и дети", Author = "И. Тургенев", Publisher = "Петербург-книга" });
            db.Books.Add(new BookModels { Name = "Чайка", Author = "А. Чехов", Publisher = "АСТ" });

            base.Seed(db);
        }
    }
}