﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.Entity;

namespace library.Models
{
    public class BookContextModels : DbContext
    {
        public DbSet<BookModels> Books { get; set; }
        public DbSet<ExtraditionModels> Extraditions { get; set; }
    }
}