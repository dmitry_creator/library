﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace library.Models
{
    public class ExtraditionModels
    {
        // ID совершения сдачи книги
        [Key]
        public int ExtraditionId { get; set; }
        // имя и фамилия читателя
        public string Person { get; set; }
        // адрес читателя
        public string Address { get; set; }
        // e=mail читателя
        public string eMail { get; set; }
        // телефон читателя
        public string TelephoneNumber { get; set; }
        // ID книги
        public int BookId { get; set; }
        // дата сдачи книги
        public DateTime Date { get; set; }
    }
}