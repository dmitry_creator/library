﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using library.Models;

namespace library.Controllers
{
    public class HomeController : Controller
    {
        // создаем контекст данных
        BookContextModels db = new BookContextModels();

        public ActionResult Index()
        {
            // получаем из бд все объекты Book
            IEnumerable<BookModels> books = db.Books;
            // передаем все объекты в динамическое свойство Books в ViewBag
            ViewBag.Books = books;
            // возвращаем представление
            return View();
        }
    }
}